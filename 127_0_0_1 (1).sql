-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2019 at 07:06 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barbershop`
--
CREATE DATABASE IF NOT EXISTS `barbershop` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `barbershop`;

-- --------------------------------------------------------

--
-- Table structure for table `detailpembayaran`
--

CREATE TABLE `detailpembayaran` (
  `nofaktur` varchar(11) NOT NULL,
  `kdpelayanan` varchar(6) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `headerpembayaran`
--

CREATE TABLE `headerpembayaran` (
  `nofaktur` varchar(11) NOT NULL,
  `tanggal` date NOT NULL,
  `namapelanggan` varchar(50) NOT NULL,
  `idpegawai` varchar(6) NOT NULL,
  `bayar` decimal(10,0) NOT NULL,
  `sisa` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `headerpembayaran`
--

INSERT INTO `headerpembayaran` (`nofaktur`, `tanggal`, `namapelanggan`, `idpegawai`, `bayar`, `sisa`, `total`) VALUES
('NT0001', '2019-05-07', 'Gani', 'PG001', '50000', '20000', '30000'),
('NT0002', '2019-05-15', 'Yuyun', 'PG002', '20000', '5000', '15000'),
('NT0003', '2019-05-22', 'Neng Imas Ulan', 'PG002', '30000', '0', '20999'),
('NT0002', '2019-05-15', 'Yuyun', 'PG002', '20000', '5000', '15000'),
('NT0003', '2019-05-22', 'Neng Imas Ulan', 'PG002', '30000', '0', '20999');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `idpegawai` varchar(6) NOT NULL,
  `namapegawai` varchar(50) NOT NULL,
  `jeniskelamin` varchar(20) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `alamatpegawai` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`idpegawai`, `namapegawai`, `jeniskelamin`, `telp`, `alamatpegawai`) VALUES
('PG002', 'Hindayanti', 'P', '0877 8191 9018', 'Jl. Mangga Manis'),
('PG003', 'Yanaf Sabrinu', 'L', '08675322346', 'Tanggerang Selatan'),
('PG004', 'Yusuf', 'L', '0877 8190 7013', 'Jl. Nangka No.8A'),
('PG005', 'Sarifah Handayani', 'P', '08776126783', 'RSN Tanah Merah Blok A No. 201'),
('PG006', 'Adang Sudrajat', 'L', '0877789230', 'Jl. KH. Ashari No.2A'),
('PG007', 'Fahran', 'L', '08777666543', 'Marunda Baru'),
('PG008', 'Paramitha Indriastuti', 'P', '087789214418', 'Jl. Teratai No.9C'),
('PG009', 'Sari', 'P', '087765432', 'meruya utara');

-- --------------------------------------------------------

--
-- Table structure for table `pelayanan`
--

CREATE TABLE `pelayanan` (
  `kdpelayanan` varchar(6) NOT NULL,
  `namapelayanan` varchar(50) NOT NULL,
  `hargapelayanan` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelayanan`
--

INSERT INTO `pelayanan` (`kdpelayanan`, `namapelayanan`, `hargapelayanan`) VALUES
('KD002', 'Pedikur Medikur', '20999'),
('KD003', 'Potong Rambut', '50000'),
('KD004', 'Cet Rambut', '25000'),
('KD005', 'Cukur Kumis', '2000'),
('KD007', 'Pijat Refleksi', '60900');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` varchar(6) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`) VALUES
('1', 'user1', '24c9e15e52afc47c225b757e7bee1f9d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detailpembayaran`
--
ALTER TABLE `detailpembayaran`
  ADD PRIMARY KEY (`nofaktur`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`idpegawai`);

--
-- Indexes for table `pelayanan`
--
ALTER TABLE `pelayanan`
  ADD PRIMARY KEY (`kdpelayanan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
