<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
   //panggil nama table
    private $_table_header = "headerpembayaran";
    private $_table_detail = "detailpembayaran";

    public function tampilDataTransaksi()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
            );
            return $query->result();  
        }

    public function saveTransaksi()
        {
            $data['nofaktur']   = $this->input->post('nofaktur');
            $data['tanggal']        = date('Y-m-d');
            $data['namapelanggan']   = $this->input->post('namapelanggan');
            $data['idpegawai']        = $this->input->post('idpegawai');
            $data['bayar']        = $this->input->post('bayar');
            $data['sisa']        = $this->input->post('sisa');
            $data['total']        = $this->input->post('total');

            $this->db->insert($this->_table_header, $data);
        }
     public function idTransaksiTerakhir()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY nofaktur DESC LIMIT 0,1"
            );
            $data_id = $query->result();

            foreach ($data_id as $data) {
                $last_id = $data->nofaktur;
            }

            return $last_id;
        }

    // public function tampilDataPenjualanDetail($id)
    //     {
    //         $query  = $this->db->query(
    //             "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN pegawai AS B ON A.idpegawai = B.idpegawai WHERE A.flag = 1 AND A.nofaktur = '$id'"
    //         );
    //         // $query = $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1");
    //         return $query->result();    
    //     }

    // public function savePenjualanDetail($id)
    //     {
    //         $idpegawai = $this->input->post('idpegawai');
    //         // $harga_barang       = $this->Barang_models->TampilHargaBarang($kode_barang);

    //         $bayar    = $this->input->post('bayar');
    //         $total  = $this->input->post('total');

    //         $data['nofaktur'] = $id;
    //         $data['idpegawai']    = $idpegawai;
    //         $data['bayar']            = $bayar;
    //         // $data['harga']          = $harga_barang;
    //         $data['total']         = $bayar - $total;
    //         $data['flag']           = 1;

    //         $this->db->insert($this->_table_detail, $data);
    //     }
// public function tampillaporanpembelian($tgl_awal,$tgl_akhir)
// public function tampillaporanpenjualan($tgl_awal, $tgl_akhir)

//     {
//         $this->db->select(' ph.idpegawai, ph.nofaktur,ph.tanggal, COUNT(pd.idpegawai) AS bayar, SUM(pd.qty) as total_qty, SUM(pd.jumlah) as total_pembelian '); 
//         $this->db->FROM ('penjualan_header ph');
//         $this->db->JOIN ('penjualan_detail pd', 'ph.id_jual_h = pd.id_jual_h');
//         $this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
//         $this->db->GROUP_BY('ph.nofaktur');
//         $query = $this->db->get();
//              return $query->result();
//           //  $_config[0] =& $config;  
//           // return $_config[0];
//     }

public function tampilDataTransaksiPagination($perpage, $uri, $data_pencarian)
    {
        // echo "<pre>";
        // print_r($data_pencarian); die();
        // echo "</pre>";
        $this->db->select('*');
        if (!empty($data_pencarian)) {
            $this->db->like('nofaktur', $data_pencarian);
        }
        $this->db->order_by('nofaktur','asc');

        $get_data = $this->db->get($this->_table_header, $perpage, $uri);
        if ($get_data->num_rows() > 0) {
            return$get_data->result();
        }else{
            return null;
        }

    }

public function tombolpagination($data_pencarian)
    {
        //echo "<pre>";
        //print_r($data_pencarian); die();
        //echo "</pre>";
        //cari jmlh data berdasarkan data pencarian
        $this->db->like('nofaktur', $data_pencarian);
        $this->db->from($this->_table_header);
        $hasil = $this->db->count_all_results();
        //echo "<pre>";
        //print_r($hasil); die();
        //echo "</pre>";

        //pagination limit
        $pagination['base_url']     = base_url().'Transaksi/listTransaksi/load/';
        $pagination['total_rows']   =$hasil;
        $pagination['per_page']     = "3";
        $pagination['uri_segment']  = 4;
        $pagination['num_links']    = 2;

        //custom pagging configuration
        $pagination['full_tag_open']    = '<div class="pagination">';
        $pagination['full_tag_close']   = '</div>';

        $pagination['first_link']       = 'First Page';
        $pagination['first_tag_open']   = '<span class="firstlink">';
        $pagination['first_tag_close']  = '</span>';

        $pagination['last_link']        = 'Last Page';
        $pagination['last_tag_open']    = '<span class="lastlink">';
        $pagination['last_tag_close']   = '</span>';

        $pagination['next_link']        = 'Next Page';
        $pagination['next_tag_open']    = '<span class="nextlink">';
        $pagination['next_tag_close']   = '</span>';

        $pagination['prev_link']        = 'Prev Page';
        $pagination['prev_tag_open']    = '<span class="prevlink">';
        $pagination['prev_tag_close']   = '</span>';

        $pagination['cur_tag_open']     = '<span class="curlink">';
        $pagination['cur_tag_close']    = '</span>';

        $pagination['num_tag_open']     = '<span class="numlink">';
        $pagination['num_tag_close']    = '</span>';

        $this->pagination->initialize($pagination);

        $hasil_pagination   = $this->tampilDataTransaksiPagination($pagination['per_page'],
        $this->uri->segment(4), $data_pencarian);

        return $hasil_pagination;
    }
public function createKodeUrut(){
    //cek kode barang terakhir
    $this->db->select('MAX(no_transaksi) as no_transaksi');
    $query  = $this->db->get($this->_table_header);
    $result = $query->row_array(); //hasil bentuk array

    $no_transaksi_terakhir = $result['no_transaksi'];
    //format BR001 = BR (label awal), 001 (nomor urut)
    $label = "PJ";
    $no_urut_lama = (int) substr($no_transaksi_terakhir, 2,3);
    $no_urut_lama ++;

    $no_urut_baru = sprintf("%03s", $no_urut_lama);
    $no_trans_baru= $label . $no_urut_baru;

    return $no_trans_baru;
    }
}
